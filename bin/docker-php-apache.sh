#!/usr/bin/env bash
#v.1.0

IMAGE="testudio/drupal8-web-test"
TAG1="latest"
PHPVER="7.2"
TAG2="php$PHPVER"
TAG3="$TAG2-$(date '+%Y%m%d')"

docker login
docker images
docker rmi $IMAGE:$TAG1 $IMAGE:$TAG2 $IMAGE:$TAG3 php:7.2-apache -f

docker build -t $IMAGE:$TAG1 ./docker/php-apache/$PHPVER

#docker tag $IMAGE:$TAG1 $IMAGE:$TAG2
#docker tag $IMAGE:$TAG1 $IMAGE:$TAG3
#
#docker push $IMAGE:$TAG1
#docker push $IMAGE:$TAG2
#docker push $IMAGE:$TAG3
