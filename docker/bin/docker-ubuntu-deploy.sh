#!/usr/bin/env bash
#v.1.2

ERROR="Parameter <ubuntu version> is not found. Must be 18.04; 19.04."
if [ $# -ne 1 ]; then
  echo $ERROR
  exit 1
else
  if [ "$1" != "18.04" ] && [ "$1" != "19.04" ]; then
    echo $ERROR
    exit 1
  fi
fi

IMAGE="testudio/ubuntu-deploy"
TAG1="latest"
TAG2="u$1"
TAG3="$TAG2-$(date '+%Y%m%d')"

docker login # user lowercase login
docker images
docker rmi $IMAGE:$TAG1 $IMAGE:$TAG2 $IMAGE:$TAG3 php:$1-apache -f || echo 'No images to delete.'

docker build -t $IMAGE:$TAG1 ./docker/ubuntu-deploy/$1 || exit 1

docker tag $IMAGE:$TAG1 $IMAGE:$TAG2
docker tag $IMAGE:$TAG1 $IMAGE:$TAG3

docker push $IMAGE:$TAG1
docker push $IMAGE:$TAG2
docker push $IMAGE:$TAG3
