#!/usr/bin/env bash
#v.1.2

ERROR="Parameter <php version> is not found. Must be 7.1; 7.2; 7.3."
if [ $# -ne 1 ]; then
  echo $ERROR
  exit 1
else
  if [ "$1" != "7.1" ] && [ "$1" != "7.2" ] && [ "$1" != "7.3" ]; then
    echo $ERROR
    exit 1
  fi
fi

IMAGE="testudio/drupal8-web-test"
TAG1="latest"
TAG2="php$1"
TAG3="$TAG2-$(date '+%Y%m%d')"

docker login # user lowercase login
docker images
docker rmi $IMAGE:$TAG1 $IMAGE:$TAG2 $IMAGE:$TAG3 php:$1-apache -f || echo 'No images to delete.'

docker build -t $IMAGE:$TAG1 ./docker/php-apache/$1 || exit 1

docker tag $IMAGE:$TAG1 $IMAGE:$TAG2
docker tag $IMAGE:$TAG1 $IMAGE:$TAG3

if [ "$1" == "7.3" ]; then
  docker push $IMAGE:$TAG1
fi
docker push $IMAGE:$TAG2
docker push $IMAGE:$TAG3
