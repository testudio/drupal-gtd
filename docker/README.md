# Docker

## Docker Hub

* [docker-ubuntu-deploy](https://cloud.docker.com/u/testudio/repository/docker/testudio/ubuntu-deploy)
* [docker-php-apache](https://cloud.docker.com/u/testudio/repository/docker/testudio/drupal8-web-test)

## Build

Run commands to build images and push to docker hub

```
bash docker/bin/docker-ubuntu-deploy.sh 18.04
bash docker/bin/docker-ubuntu-deploy.sh 19.04
bash docker/bin/docker-php-apache.sh 7.2
bash docker/bin/docker-php-apache.sh 7.3
```
