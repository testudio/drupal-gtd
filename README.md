# Drupal Global Training Day setup

* [Documentation](./docs/README.md)
  * [Training: migration](./docs/DGTD-MIGRATION.md)

## Quick start

1. Get a machine with Docker and Docker compose.

2. Pull repository

```
mkdir -p /app && cd /app
git clone https://gitlab.com/testudio/drupal-gtd.git
cd drupal-gtd
docker-compose up -d --build
```

3. `Optional`. Create startup script

```
crontab -e
@reboot cd /app/drupal-gtd && /usr/local/bin/docker-compose up -d
```

4. Install Drupal

```
docker exec -it web composer install # Get composer dependencies
docker exec -it web composer site-install-docker-ci # Install drupal
docker exec -it web composer drush-uli # Login into Drupal
# OR docker exec -it web ./vendor/bin/drush -l default user:login --uri=http://178.128.213.12
```

## Links

* [Drupal Global Training Day](https://groups.drupal.org/node/512931)
* [Drupal](https://www.drupal.org/)
