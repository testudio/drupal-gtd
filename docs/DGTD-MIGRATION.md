[`<< Back`](README.md)

# Drupal 8 training: migration helper

Migrating content to Drupal 8.

## Module 1. Simple migration.

### 1.1. Intro.

Drupal consist of 

* `configuration`
* `content`: `database data` and `files data`

### 1.2. Configuration.

Every structural change can be exported as configuration yaml file.

### Task 1.2

* Enable `Configuration Manager` module.
* Head to `Configuration Synchronization` in `Configuration`.
* Explore `system.date` simple configuration.
* Go to Date settings and change settings.
* Explore `system.date` simple configuration again.
* Export the whole configuration.

### 1.3. Configuration storage.

Configuration can be stored:

* By itself (configuration import export)
* In profiles
* In themes
* In modules (features)

Configuration is copied on installation.

### Task 1.3

* Explore profile configuration.
* Explore theme configuration.
* Explore module configuration.

### 1.4. Database.

Drupal database includes both 

* `configuration`
* `content`

### Task 1.4

* Use `phpMyAdmin` to explore configuration.
* Use `phpMyAdmin` to download database.

### 1.5. Backup and migrate module.

[Backup and migrate](https://www.drupal.org/project/backup_migrate) 
provides extended database backup.

### Task 1.5

* Enable `Backup and migrate` module.
* Explore `Backup and migrate` configuration.

## Module 2. Creating and exporting content.

### 2.1. Default content module.

[Default Content for D8](https://www.drupal.org/project/default_content) 
provides support for exporting/importing content.

For files support use [XXX]().

### Task 2.1

* Enable `Default Content for D8 example` module.
* Explore `Default Content for D8 example` configuration.

### 2.2. Features module.

[Features](https://www.drupal.org/project/features) 
provides support for exporting files.

### Task 2.2

TBA

### 2.3. JSON Feed module.

[JSON Feed](https://www.drupal.org/project/json_feed) 
provides support for exporting files.

### Task 2.3

TBA

### 2.4. Webform and CSV.

[Webform](https://www.drupal.org/project/webform) module
provides support for exporting files.

### Task 2.4

TBA

## Module 3. Import data using Feeds modules module.

### 3.1. Importing CSV.

TBA

### Task 3.1

TBA

### 3.2. Importing JSON.

TBA

### Task 3.2

TBA


### 3.3. Connecting imports.

TBA

### Task 3.3

TBA

## Module 4. Import data using core migrate API modules.

### 4.1. Importing JSON.

TBA

### Task 4.1

TBA


### 4.2. Connecting imports.

TBA

### Task 4.2

TBA

### 4.3. Importing CSV.

TBA

### Task 4.3

TBA

## Homework 1. Extending migrate API functionality.

TBA

## Homework 2. Contributing to migrate API.

TBA

[`<< Back`](README.md)
