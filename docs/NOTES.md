# Drupal 8 for managers

## Drupal

* [Druapl.org](https://www.drupal.org/)
* [Druapl showcases](https://www.drupal.com/showcases)

### Modules in use during training

* Admin toolbar
* Coffee
* Bootstrap theme
* Display Suite
* Webform
* Backup and migrate

### Drupal training / courses

* TBA

### Drupal in Australia

* [GovCMS](https://www.govcms.gov.au/)
* [aGov](https://agov.com.au/)
* [Salsa Digital and amazee.io to build next generation GovCMS platform](https://salsadigital.com.au/news/salsa-digital-and-amazeeio-build-next-generation-govcms-platform)

### Hosting

#### Dedicated Drupal hosting

* [Acquia](https://www.acquia.com/)
  * Present in Australia
  * Free development trial
  
* [Platfrom.sh](https://platform.sh/)
  * Present in Australia
  * Free development trial
  
* [Pantheon](https://pantheon.io/)
  * Not present in Australia
  * Free development tier
  
#### Other hosting

[Amazon Lightsail](https://aws.amazon.com/lightsail/)
  * Contains one click Drupal server
  * Present in Australia
  
[Linode](https://www.linode.com/)
  * Not present in Australia
  
[Digital Ocean](https://www.digitalocean.com/)
  * No longer contains one click Drupal server
  * Not present in Australia
  
[Google Cloud](https://console.cloud.google.com/)
  * Present in Australia
  * Enterprise boxes on Marketplace 
  
Edited: 02 September 2018
