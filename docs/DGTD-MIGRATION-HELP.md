[`<< Back`](README.md)

# Drupal 8 training: migration

Migrating content to Drupal 8.

## Module 1. Simple migration.

### 1.1. Intro

Drupal consist of 

* `configuration`
* `content`: `database data` and `files data`

### 1.2. Configuration

Every structural change can be exported as configuration yaml file.

### Task 1.1

* Enable `Configuration Manager` module.
* Head to `Configuration`.
* 

### Backup and migrate.

TBA

### Backup and migrate.

TBA

## Tutorial 1.

TBA

## Module 2. Creating and exporting content.

TBA

## Tutorial 2.

TBA

## Module 3. Import data using Feeds modules module.

TBA

## Tutorial 3.

TBA

## Module 4. Import data using core migrate API modules.

TBA

## Tutorial 4.

TBA

## Homework 1. Extending migrate API functionality.

TBA

## Homework tutorial 1.

TBA

## Homework 2. Contributing to migrate API.

TBA

## Homework tutorial 2.

TBA

[`<< Back`](README.md)
