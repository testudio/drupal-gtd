# Help

## Docker 

You can use scripts to build and commit images located in `docker/bin/.`

### Building and committing

To build updated image run

```
docker build -t "testudio/drupal8-web-test" ./docker/php-apache/
rm docker/config/composer*
rm -rf  docker/config/scripts
```

To test running image

```
docker run -it --rm testudio/drupal8-web-test [COMMAND]
```

To push updated image run

```
docker login # Only if not logged in

PHPVER="php7.3"
docker push testudio/drupal8-web-test:latest
docker image tag testudio/drupal8-web-test testudio/drupal8-web-test:$PHPVER-$(date '+%Y%m%d')
docker push testudio/drupal8-web-test:$PHPVER-$(date '+%Y%m%d')
docker image tag testudio/drupal8-web-test testudio/drupal8-web-test:$PHPVER
docker push testudio/drupal8-web-test:$PHPVER
```