# To be reviewed 

## Cloud config 

https://cloudinit.readthedocs.io/en/latest/topics/examples.html#configure-instances-ssh-keys

```
#cloud-config
repo_update: true
repo_upgrade: all

package_update: true
package_upgrade: true

packages:
  - apt-transport-https
  - ca-certificates
  - git
  - curl
  - software-properties-common
  - docker.io
  
runcmd:
  - apt update
  - apt install -y docker-ce 
  - apt install -y docker-compose
  - apt autoremove -y
  - cd /root && git clone https://gitlab.com/testudio/drupal-gtd.git app
  - cd /root/app && mkdir web
  - cd /root/app && docker-compose up -d --build
  - cd /root/app && docker exec -it web composer install
  - cd /root/app && docker exec -it web composer site-install-docker-ci
  - cd /root/app && docker exec -it web composer require drupal/bootstrap_barrio drupal/ds drupal/adminimal_theme


docker exec -it web ./vendor/bin/drush --root=$(pwd)/web -y config-set system.site name 'Drupal library'
docker exec -it web ./vendor/bin/drush --root=$(pwd)/web en -y ds bootstrap adminal
docker exec -it web ./vendor/bin/drush --root=$(pwd)/web user:password admin 'drupal'

final_message: "The system is finally up, after $UPTIME seconds"

bootcmd:
 - cd /root/app && docker-compose restart
```